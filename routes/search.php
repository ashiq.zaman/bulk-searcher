<?php

use App\Http\Controllers\BulkSearchUploadController;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'search',
    'as' => 'search.',
    'middleware' => 'auth',
], function () {
    Route::get('', [BulkSearchUploadController::class, 'index'])->name('index');
    Route::get('/raw/{id}/url', [BulkSearchUploadController::class, 'rawUrl'])->name('raw.url');
    Route::post('', [BulkSearchUploadController::class, 'upload'])->name('upload');
});
