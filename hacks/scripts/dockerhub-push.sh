#!/bin/bash

PROJECT_DIR=$(dirname "$(dirname "$(dirname "$(readlink -f "$0")")")")
TAG_NAME="ashiquzzaman33/bulk-searcher:latest"
docker build -t $TAG_NAME -f $PROJECT_DIR/hacks/docker/8.2/Dockerfile.prod $PROJECT_DIR
docker push $TAG_NAME

echo "Successfully pushed $TAG_NAME"
