<?php

use App\Searcher\Parsers\CsvKeywordParser;

return [
    'parsers' => [
        'csv' => CsvKeywordParser::class,
    ],
    'kafka_scheduled_search_topic_name' => env('KAFKA_SCHEDULED_SEARCH_TOPIC_NAME', 'google_scheduled_search_raw'),
    'kafka_search_done_topic_name' => env('KAFKA_SEARCH_DONE_TOPIC_NAME', 'google_scheduled_search_done'),
];
