<?php

namespace Database\Factories;

use App\Models\SearchResult;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Search>
 */
class SearchResultFactory extends Factory
{
    protected $model = SearchResult::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $status = fake()->numberBetween(1, 3);
        $error = '';
        if ($status == SearchResult::STATUS_SEARCH_FAILED) {
            $error = 'search failed due to too many search';
        }

        return [
            'keyword' => fake()->sentence,
            'ads_count' => fake()->numberBetween(0, 5),
            'links_count' => fake()->numberBetween(1, 10),
            'total_search_count' => fake()->numberBetween(100000, 10000000),
            'raw_search_page' => fake()->randomHtml,
            'searched_at' => fake()->dateTimeBetween(),
            'status' => $status,
            'error' => $error,
        ];
    }
}
