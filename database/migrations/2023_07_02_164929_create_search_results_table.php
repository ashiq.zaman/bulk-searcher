<?php

use App\Models\SearchResult;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('search_results', function (Blueprint $table) {
            $table->id();
            $table->string('keyword', 1024)->unique();
            $table->unsignedInteger('ads_count')->nullable();
            $table->unsignedInteger('links_count')->nullable();
            $table->unsignedBigInteger('total_search_count')->nullable();
            $table->longText('raw_search_page')->nullable();
            $table->unsignedSmallInteger('status')->default(SearchResult::STATUS_TO_BE_SEARCHED);
            $table->string('error')->nullable();
            $table->dateTime('searched_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('search_results');
    }
};
