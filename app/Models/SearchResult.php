<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SearchResult extends Model
{
    use HasFactory;

    protected $table = 'search_results';

    public const STATUS_TO_BE_SEARCHED = 1;

    public const STATUS_SEARCH_FINISHED = 2;

    public const STATUS_SEARCH_FAILED = 3;

    protected $fillable = [
        'keyword',
        'ads_count',
        'links_count',
        'total_search_count',
        'raw_search_page',
        'status',
        'error',
        'searched_at',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'search_result_user');
    }

    public function isDone()
    {
        return $this->status == self::STATUS_SEARCH_FINISHED;
    }

    public function isPending()
    {
        return $this->status == self::STATUS_TO_BE_SEARCHED;
    }

    public function getStatusAsStringAttribute()
    {
        switch ($this->status) {
            case self::STATUS_TO_BE_SEARCHED:
                return 'waiting';
            case self::STATUS_SEARCH_FINISHED:
                return 'done';
            case self::STATUS_SEARCH_FAILED:
                return 'failed';
        }
    }
}
