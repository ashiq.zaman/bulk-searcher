<?php

namespace App\Console\Commands;

use App\Models\SearchResult;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Junges\Kafka\Kafka;
use Junges\Kafka\Message\ConsumedMessage;
use Psr\Log\LoggerInterface;

class ListenSearchDoneCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:listen-search-done-command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public function __construct(private LoggerInterface $logger)
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(Kafka $kafka)
    {
        $this->logger->info('search done listener started, topic: '.config('kafka.consumer_group_id'));
        $kafka->createConsumer([config('searcher.kafka_search_done_topic_name')])
            ->withConsumerGroupId(config('kafka.consumer_group_id'))
            ->withHandler(function (ConsumedMessage $message) {
                try {
                    $body = $message->getBody();
                    $searchResult = SearchResult::findOrFail($body['id']);

                    if ($body['status'] == 'failed') {
                        $searchResult->error = $body['error'];
                        $searchResult->status = SearchResult::STATUS_SEARCH_FAILED;
                        $searchResult->searched_at = Carbon::now();
                    } else {
                        $searchResult->raw_search_page = e($body['page']);
                        $searchResult->ads_count = $body['ads_count'];
                        $searchResult->links_count = $body['links_count'];
                        $searchResult->total_search_count = $body['total_search_count'];
                        $searchResult->searched_at = Carbon::now();
                        $searchResult->status = SearchResult::STATUS_SEARCH_FINISHED;
                    }
                    $searchResult->save();
                    $this->logger->info('search done', [
                        'id' => $searchResult->id,
                        'keyword' => $searchResult->keyword,
                        'links_count' => $searchResult->links_count,
                    ]);
                } catch (\Exception $exception) {
                    $this->logger->error('search done consume error ', ['error' => $exception->getMessage()]);
                }
            })
            ->build()
            ->consume();
    }
}
