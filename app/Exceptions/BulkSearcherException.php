<?php

namespace App\Exceptions;

use Exception;

class BulkSearcherException extends Exception
{
    protected $code = 400;
}
