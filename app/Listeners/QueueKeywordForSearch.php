<?php

namespace App\Listeners;

use App\Events\NewSearchKeywordFound;
use Junges\Kafka\Facades\Kafka;
use Junges\Kafka\Message\Message;
use Psr\Log\LoggerInterface;

class QueueKeywordForSearch
{
    /**
     * Create the event listener.
     */
    public function __construct(private LoggerInterface $logger)
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(NewSearchKeywordFound $event): void
    {
        Kafka::publishOn(config('searcher.kafka_scheduled_search_topic_name'))->withMessage(Message::create()->withBody([
            'id' => $event->searchResult->id,
            'keyword' => $event->searchResult->keyword,
        ]))->send();
        $this->logger->info("search keyword '".$event->searchResult->keyword."' published on topic ".config('searcher.kafka_scheduled_search_topic_name'));
    }
}
