<?php

namespace App\Searcher;

use App\Events\NewSearchKeywordFound;
use App\Models\SearchResult;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class BulkSearchScheduler
{
    public function scheduleKeywords(Collection $keywords): void
    {
        $keywords->each(function ($keyword) {
            $searchResult = SearchResult::updateOrCreate([
                'keyword' => $keyword,
            ]);
            if ($searchResult->wasRecentlyCreated) {
                NewSearchKeywordFound::dispatch($searchResult);
            }
            $searchResult->users()->syncWithoutDetaching(Auth::id());
        });
    }
}
