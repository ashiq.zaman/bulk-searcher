<?php

namespace App\Searcher\Parsers;

use App\Exceptions\TooLargeKeywordException;
use App\Exceptions\TooManySearchKeywordsException;
use Illuminate\Support\Collection;

interface KeywordParser
{
    /**
     * @throws TooManySearchKeywordsException
     * @throws TooLargeKeywordException
     */
    public function getKeywords(): Collection;
}
