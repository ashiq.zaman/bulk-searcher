<?php

namespace App\Searcher\Parsers;

use App\Exceptions\KeywordParserNotFoundException;
use Illuminate\Http\UploadedFile;

class KeywordParserFactory
{
    /**
     * @throws KeywordParserNotFoundException
     */
    public function getKeywordParser(UploadedFile $file): KeywordParser
    {
        $parsers = config('searcher.parsers');
        if (! array_key_exists($file->getClientOriginalExtension(), $parsers)) {
            throw new KeywordParserNotFoundException('parser not found for '.$file->getClientOriginalExtension().' file type');
        }

        return new $parsers[$file->getClientOriginalExtension()]($file);
    }
}
