<?php

namespace App\Searcher\Parsers;

use App\Exceptions\TooLargeKeywordException;
use App\Exceptions\TooManySearchKeywordsException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;

class CsvKeywordParser implements KeywordParser
{
    public const MAX_KEYWORD_LENGTH = 1024;

    public const MAX_NUMBER_OF_KEYWORDS = 100;

    public function __construct(private readonly UploadedFile $uploadedFile)
    {

    }

    /**
     * @throws TooManySearchKeywordsException
     * @throws TooLargeKeywordException
     */
    public function getKeywords(): Collection
    {
        $keywords = collect(explode(',', $this->uploadedFile->getContent()))->map(function ($keyword) {
            return trim($keyword);
        })->filter(function ($keyword) {
            if (strlen($keyword) > self::MAX_KEYWORD_LENGTH) {
                throw new TooLargeKeywordException('maximum size of each keyword is '.self::MAX_KEYWORD_LENGTH.' characters long');
            }

            return ! empty($keyword);
        })
            ->values();
        if (count($keywords) > self::MAX_NUMBER_OF_KEYWORDS) {
            throw new TooManySearchKeywordsException('maximum number of keywords in each file is '.self::MAX_NUMBER_OF_KEYWORDS);
        }

        return $keywords;
    }
}
