<?php

namespace App\Http\Controllers;

use App\Exceptions\KeywordParserNotFoundException;
use App\Exceptions\TooLargeKeywordException;
use App\Exceptions\TooManySearchKeywordsException;
use App\Models\SearchResult;
use App\Searcher\BulkSearchScheduler;
use App\Searcher\Parsers\KeywordParserFactory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class BulkSearchUploadController extends Controller
{
    public function __construct(private readonly KeywordParserFactory $keywordParserFactory,
        private readonly BulkSearchScheduler $searchKeywordHandler)
    {
    }

    public function index(Request $request)
    {
        $searchResults = Auth::user()->searches();

        if ($request->has('keyword')) {
            $searchResults->where('keyword', 'ILIKE', '%'.$request->get('keyword').'%');
        }

        return view('search.index')->with([
            'searchResults' => $searchResults->select([
                'id',
                'keyword',
                'ads_count',
                'total_search_count',
                'status',
                'error',
                'searched_at',
            ])->orderBy('created_at', 'desc')
                ->paginate(5),
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function upload(Request $request): RedirectResponse
    {
        $this->validate($request, [
            'keywords_file' => 'required|file|mimetypes:text/csv,text/plain|max:1024',
        ]);
        try {
            $keywords = $this->keywordParserFactory->getKeywordParser($request->file('keywords_file'))->getKeywords();
            $this->searchKeywordHandler->scheduleKeywords($keywords);

            return Redirect::route('search.index')->with('status', 'search-scheduled');
        } catch (TooLargeKeywordException|TooManySearchKeywordsException|KeywordParserNotFoundException $e) {
            throw ValidationException::withMessages([
                'keywords_file' => Str::ucfirst($e->getMessage()),
            ]);
        }
    }

    public function rawUrl($id)
    {
        $searchResult = SearchResult::findOrFail($id);
        return html_entity_decode($searchResult->raw_search_page);
    }
}
