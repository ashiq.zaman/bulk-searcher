<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\KeywordParserNotFoundException;
use App\Exceptions\TooLargeKeywordException;
use App\Exceptions\TooManySearchKeywordsException;
use App\Http\Controllers\Controller;
use App\Models\SearchResult;
use App\Searcher\BulkSearchScheduler;
use App\Searcher\Parsers\KeywordParserFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class SearchController extends Controller
{
    public function __construct(private readonly KeywordParserFactory $keywordParserFactory,
                                private readonly BulkSearchScheduler  $searchKeywordHandler)
    {
    }

    public function keywords(Request $request): JsonResponse
    {
        $perPage = $request->get('per_page', 10);
        $orderBy = $request->get('order');
        if ($orderBy != 'asc') {
            $orderBy = 'desc';
        }
        $result = SearchResult::select('keyword')
            ->orderBy('created_at', $orderBy)
            ->paginate($perPage);

        return response()->json([
            'data' => array_map(fn($item) => $item['keyword'], $result->items()),
            'total' => $result->total(),
            'per_page' => $result->perPage(),
            'page' => $result->currentPage(),
        ]);
    }

    public function keyword(string $keyword): Response|JsonResponse
    {
        $result = SearchResult::where('keyword', trim($keyword))->first();
        if (empty($result)) {
            return response()->noContent(404);
        }

        return response()->json($result);
    }

    /**
     * @throws ValidationException
     */
    public function upload(Request $request): RedirectResponse|JsonResponse
    {
        $this->validate($request, [
            'keywords_file' => 'required|file|mimetypes:text/csv,text/plain|max:1024',
        ]);
        try {
            $keywords = $this->keywordParserFactory->getKeywordParser($request->file('keywords_file'))->getKeywords();
            $this->searchKeywordHandler->scheduleKeywords($keywords);
            return response()->json(['message' => 'success']);


        } catch (TooLargeKeywordException|TooManySearchKeywordsException|KeywordParserNotFoundException $e) {
            throw ValidationException::withMessages([
                'keywords_file' => Str::ucfirst($e->getMessage()),
            ]);
        }
    }
}
