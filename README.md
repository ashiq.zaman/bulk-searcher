# Bulk Searcher

A web application that will allow bulk keywords search using search engine.

### Requirements

- Laravel 10
- Docker
- PHP 8.2
- Node

### Installation and Run

1. Clone the repository:

```bash
git git@gitlab.com:ashiq.zaman/bulk-searcher.git
cd bulk-searcher
```

2. Install dependencies and build

```bash
sail composer update
npm run install
npm run build
```

3. Run

```bash
sail up
```

4. Tear down

```bash
sail down
```

#### Testing

```bash
./vendor/bin/sail test
```

### Deployment using docker swarm
Push dokcer image to dockerhub
```bash
./hacks/scripts/dockerhub-push.sh
```
Tools
```bash
# Copy hacks/deployments/toos.yaml to server
export NODE_PUBLIC_IP_OR_DOMAIN=172.31.35.123
export DB_DATABASE=bulk_searcher
export DB_PASSWORD=aaaaaa
export DB_USERNAME=bulk_searcher
docker stack deploy -c ./tools.yaml bulk-searcher
```
Bulk searcher
```bask
# copy .env to servers /data/app/.env and update with appropriate values
# copy bulk-searcher.yaml to server
docker stack deploy -c bulk-searcher.yaml bulk-searcher
```

# TODO
1. Add CI/CD
2. Use websocket/short polling in frontend to automatically update search result
3. Add retry button in search result
4. Add more test

