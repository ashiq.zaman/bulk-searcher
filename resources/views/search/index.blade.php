<x-app-layout>
    <div class="py-12" x-data="{ iframeSrcTemplate: '{{route('search.raw.url', ['id' => ':id'])}}', iframeSrc: ''}">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <header class="mb-4">
                    <h4 class="text-lg font-medium text-gray-900">
                        {{ __('Bulk Upload') }}
                    </h4>
                </header>

                <div class="max-w-xl">
                    <form class="mb-3" action="{{route('search.upload')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="flex items-center mb-3" x-data="{fileLabel: 'Choose Keywords File'}">
                            <input type="file" id="csvFile" name="keywords_file" class="hidden" accept=".csv"
                                   x-on:change=" fileLabel = $event.target.files[0].name">
                            <label for="csvFile"
                                   class="w-full sm:w-auto flex items-center justify-center px-4 py-2 border border-gray-300 rounded-l-md bg-white text-gray-600 hover:text-gray-900 cursor-pointer">
                                <span id="fileLabel" x-text="fileLabel">Choose Keywords File</span>
                            </label>
                            <x-input-attatched-button type="submit" id="upload">{{ __('Upload') }}
                            </x-input-attatched-button>
                        </div>
                    </form>
                    <x-input-error :messages="$errors->get('keywords_file')" class="mt-2"/>
                    @if (session('status') === 'search-scheduled')
                    <p
                        x-data="{ show: true }"
                        x-show="show"
                        x-transition
                        x-init="setTimeout(() => show = false, 4000)"
                        class="text-sm text-green-600"
                    >{{ __('Bulk keywords scheduled for search.') }}</p>
                    @endif
                </div>
            </div>

            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <header>
                    <h5 class="text-lg font-medium text-gray-900">
                        {{ __('Search History') }}
                    </h5>
                </header>
                <div class="flex flex-col mt-8">
                    <form action="{{route('search.index')}}">
                        <div class="flex mb-4">
                            <input type="text"
                                   class="w-full sm:w-64 px-4 py-2 border border-gray-300 rounded-l-md focus:outline-none"
                                   placeholder="Search Keyword"
                                   value="{{request()->input('keyword')}}"
                                   name="keyword">
                            <x-input-attatched-button type="submit" id="button-search">{{ __('Search') }}
                            </x-input-attatched-button>
                        </div>
                    </form>
                    <div class="overflow-x-auto">
                        <div class="inline-block min-w-full">
                            <div class="table-wrapper" style="overflow-x: auto; max-width: 100%">
                                <table class="w-full bg-white border border-gray-300">
                                    <thead>
                                    <tr>
                                        <th class="px-6 py-4 border-b text-left">Keyword</th>
                                        <th class="px-6 py-4 border-b text-left">Total AdWords</th>
                                        <th class="px-6 py-4 border-b text-left">Total Links</th>
                                        <th class="px-6 py-4 border-b text-left">Total Search Result</th>
                                        <th class="px-6 py-4 border-b text-left">Search Result Updated At</th>
                                        <th class="px-6 py-4 border-b text-left">Status</th>
                                        <th class="px-6 py-4 border-b text-left">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($searchResults as $searchResult)
                                    <tr>
                                        <td class="px-6 py-4 border-b">{{$searchResult->keyword}}</td>
                                        <td class="px-6 py-4 border-b">{{$searchResult->ads_count}}</td>
                                        <td class="px-6 py-4 border-b">{{$searchResult->links_count}}</td>
                                        <td class="px-6 py-4 border-b">{{$searchResult->total_search_count}}</td>
                                        <td class="px-6 py-4 border-b">{{$searchResult->searched_at}}</td>
                                        <td class="px-6 py-4 border-b">{{$searchResult->statusAsString}}</td>
                                        <td class="px-6 py-4 border-b">
                                            @if($searchResult->isDone())
                                            <x-primary-button
                                                x-data=""
                                                x-on:click.prevent="iframeSrc = iframeSrcTemplate.replace(':id', '{{$searchResult->id}}'); $dispatch('open-modal', 'view-search-history')"
                                            >{{ __('Show Search Page') }}
                                            </x-primary-button>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4 flex justify-center">
                    <div class="flex justify-center">
                        {{$searchResults->links()}}
                    </div>
                </div>
            </div>
        </div>
        <x-modal name="view-search-history" focusable id="view-search-history" x-data="" hscreen="h-screen">
            <div class="w-full h-full">
                <iframe x-bind:src="iframeSrc" class="w-full h-full"></iframe>
            </div>
        </x-modal>
    </div>
</x-app-layout>
