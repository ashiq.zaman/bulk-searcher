<button {{ $attributes->merge(['type' => 'button', 'class' => 'px-4 py-2 rounded-r-md hover:bg-sky-700 focus:outline-none bg-sky-600 text-white focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 transition ease-in-out duration-150']) }}>
    {{ $slot }}
</button>
