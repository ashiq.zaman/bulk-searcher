<?php

namespace Tests\Unit;

use App\Exceptions\TooLargeKeywordException;
use App\Exceptions\TooManySearchKeywordsException;
use App\Searcher\Parsers\CsvKeywordParser;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use PHPUnit\Framework\TestCase;

class CsvKeywordParserTest extends TestCase
{
    public function test_parser_can_parse_valid_file_correctly(): void
    {
        $this->assertEquals(collect([
            'cat smile',
            'playful dog',
            'cat with white fur',
        ]), $this->getParser('cat smile,playful dog,cat with white fur')->getKeywords());
    }

    public function test_parser_can_parse_valid_file_with_extra_whitespaces_correctly(): void
    {
        $this->assertEquals(collect([
            'cat smile',
            'playful dog',
            'cat with white fur',
        ]), $this->getParser('     cat smile,playful dog,cat with white fur       ')->getKeywords());
    }

    public function test_parser_can_parse_valid_file_with_empty_keyword_correctly(): void
    {
        $this->assertEquals(collect([
            'cat smile',
            'playful dog',
            'cat with white fur',
        ]), $this->getParser(',   ,,,,     cat smile,playful dog,,cat with white fur       ,      ,')->getKeywords());
    }

    public function test_parser_should_return_empty_collection_when_parsing_empty_file()
    {
        $this->assertEquals(collect(), $this->getParser(',,,')->getKeywords());
    }

    public function test_parser_should_throw_exception_if_keyword_size_exceeds_max_lenght()
    {
        $content = 'first,'.Str::random(1025).',third';
        $this->expectException(TooLargeKeywordException::class);
        $this->getParser($content)->getKeywords();
    }

    public function test_parser_should_throw_exception_if_number_of_keywords_exceeds_max_allowed_keywords_number()
    {
        $content = '';
        for ($i = 0; $i <= CsvKeywordParser::MAX_NUMBER_OF_KEYWORDS; $i++) {
            $content .= "keyword$i,";
        }
        $this->expectException(TooManySearchKeywordsException::class);
        $this->getParser($content)->getKeywords();
    }

    private function getParser($content): CsvKeywordParser
    {
        $uploadedFile = UploadedFile::fake()->createWithContent('keywords_file', $content);

        return new CsvKeywordParser($uploadedFile);
    }
}
