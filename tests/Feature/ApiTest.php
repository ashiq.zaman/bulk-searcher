<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class ApiTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_login_with_valid_credentials(): void
    {
        $user = User::factory()->create([
            'password' => Hash::make('12345678'),
        ]);
        $response = $this
            ->json('post', '/api/login', [
                'email' => $user->email,
                'password' => '12345678',
            ]);
        $response->assertOk();
        $response->assertSee('access_token');
    }

    public function test_user_login_failed_with_invalid_credentials(): void
    {
        $user = User::factory()->create([
            'password' => Hash::make('12345678'),
        ]);
        $response = $this
            ->json('post', '/api/login', [
                'email' => $user->email,
                'password' => 'password',
            ]);
        $response->assertUnauthorized();
    }
}
