<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class BulkSearchHistoryTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Event::fake();
    }

    public function test_user_should_see_search_history(): void
    {
        $user = User::factory()->create();
        $response = $this
            ->actingAs($user)
            ->get('/search');
        $response->assertOk();
        $response->assertSee('Search History');
        $response->assertSee('Search Keyword');
    }

    public function test_user_should_see_only_own_search_history()
    {
        $user1 = User::factory()->create();
        $this
            ->actingAs($user1)
            ->post('/search', [
                'keywords_file' => UploadedFile::fake()->createWithContent('test.csv', 'The white cat,, Black Dog,Common Dear'),
            ]);
        $user2 = User::factory()->create();
        $this
            ->actingAs($user2)
            ->post('/search', [
                'keywords_file' => UploadedFile::fake()->createWithContent('test.csv', 'Moon,, Sun, Saturn,Common Dear'),
            ]);

        $response = $this
            ->actingAs($user1)
            ->get('/search');
        $response->assertSee('The white cat')
            ->assertSee('Black Dog')
            ->assertSee('Common Dear')
            ->assertDontSee('Moon')
            ->assertDontSee('Sun')
            ->assertDontSee('Saturn');

        $response = $this
            ->actingAs($user2)
            ->get('/search');
        $response->assertDontSee('The white cat')
            ->assertDontSee('Black Dog')
            ->assertSee('Moon')
            ->assertSee('Sun')
            ->assertSee('Saturn')
            ->assertSee('Common Dear');
    }

    public function test_user_can_filter_search_history_by_keyword()
    {
        $user = User::factory()->create();
        $this
            ->actingAs($user)
            ->post('/search', [
                'keywords_file' => UploadedFile::fake()->createWithContent('test.csv', 'The white cat,, Black Dog,Common Dear,Black See,Debra'),
            ]);
        $response = $this->actingAs($user)->get('/search?keyword=Black');
        $response->assertSee('Black See')
            ->assertSee('Black Dog')
            ->assertDontSee('The white cat')
            ->assertDontSee('Debra');
    }
}
