<?php

namespace Tests\Feature;

use App\Models\SearchResult;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Str;
use Tests\TestCase;

class BulkSearchUploadTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Event::fake();
    }

    public function test_authenticated_user_should_see_bulk_upload_page(): void
    {
        $user = User::factory()->create();
        $response = $this
            ->actingAs($user)
            ->get('/search');
        $response->assertOk();
        $response->assertSee('Bulk Upload');
        $response->assertSee('Choose Keywords File');
    }

    public function test_can_upload_valid_csv(): void
    {
        $user = User::factory()->create();
        $response = $this
            ->actingAs($user)
            ->post('/search', [
                'keywords_file' => UploadedFile::fake()->createWithContent('test.csv', 'The white cat,, Black Dog,'),
            ]);
        $response->assertSessionHasNoErrors()
            ->assertSessionHas('status', 'search-scheduled')
            ->assertRedirectToRoute('search.index');

        $this->assertEquals([
            'The white cat',
            'Black Dog',
        ], $user->searches()->pluck('keyword')->toArray());
    }

    public function test_should_return_error_if_keyword_length_is_greater_than_max_length(): void
    {
        $user = User::factory()->create();
        $response = $this
            ->actingAs($user)
            ->post('/search', [
                'keywords_file' => UploadedFile::fake()->createWithContent('test.csv', Str::random(2000)),
            ]);
        $response->assertInvalid(['keywords_file' => 'Maximum size of each keyword is']);
    }

    public function test_should_return_validation_error_response_for_non_csv_file(): void
    {
        $user = User::factory()->create();
        $response = $this
            ->actingAs($user)
            ->post('/search', [
                'keywords_file' => UploadedFile::fake()->createWithContent('test.xlsx', ''),
            ]);
        $response->assertInvalid(['keywords_file' => 'The keywords file field must be a file of type:']);
    }

    public function test_should_return_validation_error_response_for_no_file_supplied(): void
    {
        $user = User::factory()->create();
        $response = $this
            ->actingAs($user)
            ->post('/search', [

            ]);
        $response->assertInvalid(['keywords_file' => 'The keywords file field is required']);
    }

    public function test_should_redirect_to_login_if_not_authenticated(): void
    {
        $response = $this
            ->post('/search', [
                'keywords_file' => UploadedFile::fake()->createWithContent('test.csv', 'The white cat,, Black Dog,'),
            ]);
        $response->assertRedirect('/login');
    }

    public function test_should_return_unauthenticated_if_not_authenticated(): void
    {
        $response = $this
            ->post('/search', [
                'keywords_file' => UploadedFile::fake()->createWithContent('test.csv', 'The white cat,, Black Dog,'),
            ]);
        $response->assertRedirectToRoute('login');
    }

    public function test_keyword_should_be_unique()
    {
        $user = User::factory()->create();
        $this
            ->actingAs($user)
            ->post('/search', [
                'keywords_file' => UploadedFile::fake()->createWithContent('test.csv', 'The white cat,, Black Dog, Black Dog, Black Dog'),
            ]);
        $this->assertEquals(2, SearchResult::count());
    }

    public function test_a_keyword_should_be_attatch_with_user_single_time()
    {
        $user = User::factory()->create();
        $this
            ->actingAs($user)
            ->post('/search', [
                'keywords_file' => UploadedFile::fake()->createWithContent('test.csv', 'The white cat,, Black Dog, Black Dog, Black Dog'),
            ]);
        $this
            ->actingAs($user)
            ->post('/search', [
                'keywords_file' => UploadedFile::fake()->createWithContent('test.csv', 'The white cat,, Black Dog, Black Dog, Black Dog,new keyword'),
            ]);
        $this->assertEquals(3, SearchResult::count());
    }
}
